//
// Created by amihaila on 27-Oct-18.
//

#include <windows.h>
#include <stdio.h>
#include <tlhelp32.h>
#include <tchar.h>
#include <conio.h>

#define BUF_SIZE 1024 * 1024 * 10
TCHAR szName[]=TEXT("processes_data");
HANDLE hMapFile;
LPCTSTR pBuf;
CHAR p_data[BUF_SIZE];

void map_file() {
    hMapFile = CreateFileMapping(
            INVALID_HANDLE_VALUE,    // use paging file
            NULL,                    // default security
            PAGE_READWRITE,          // read/write access
            0,                       // maximum object size (high-order DWORD)
            BUF_SIZE,                // maximum object size (low-order DWORD)
            szName);                 // name of mapping object

    if (hMapFile == NULL) {
        printf(TEXT("Could not create file mapping object (%d).\n"), GetLastError());
        return;
    }

    pBuf = (LPTSTR) MapViewOfFile(hMapFile,   // handle to map object
                                  FILE_MAP_ALL_ACCESS, // read/write permission
                                  0,
                                  0,
                                  BUF_SIZE);

    if (pBuf == NULL) {
        printf(TEXT("Could not map view of file (%d).\n"), GetLastError());
        CloseHandle(hMapFile);
        return;
    }
}

void print_all_processes() {
    HANDLE hProcessSnap;
    PROCESSENTRY32 pe32;

    //cer un snapshot la procese
    hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if(hProcessSnap == INVALID_HANDLE_VALUE) {
        printf("CreateToolhelp32Snapshot failed.err = %d \n", GetLastError());
        return;
    }

    //initializez dwSize cu dimensiunea structurii.
    pe32.dwSize = sizeof(PROCESSENTRY32);
//    //obtin informatii despre primul proces
    if(!Process32First(hProcessSnap, &pe32)) {
        printf("Process32First failed. err = %d \n", GetLastError() );
        CloseHandle(hProcessSnap); //inchidem snapshot-ul
        return;
    }

    do {
        //afisez pid-ul si executabilul
        TCHAR line[256];
        sprintf_s(line, 256, "%d,%d,%s\n", pe32.th32ParentProcessID, pe32.th32ProcessID, pe32.szExeFile);
        strcat_s(p_data, BUF_SIZE, line);
    }
    while(Process32Next(hProcessSnap, &pe32)); //trec la urmatorul proces

    CopyMemory((PVOID)pBuf, p_data, (strlen(p_data) * sizeof(TCHAR)));
    _getch();
    //inchid handle-ul catre snapshot
    UnmapViewOfFile(pBuf);
    CloseHandle(hMapFile);
    CloseHandle(hProcessSnap);
}

int main() {
    printf_s("Hello, world!\n\n");
    map_file();
    print_all_processes();
    return 0;
}

