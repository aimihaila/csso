#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <cstdlib>
#include <map>
#include <set>
#include <vector>
#include <iostream>

#pragma comment(lib, "user32.lib")
using namespace std;

#define BUF_SIZE 1024 * 1024 * 10
TCHAR szName[]=TEXT("processes_data");
map <int, char*> children_names;
map <int, int> children_parents;
map <int, vector<int>> parents_children;
set<int> roots;


void set_up_privileges() {
    HANDLE current_process_token;
    LUID privilege_id;
    TOKEN_PRIVILEGES privileges;

    if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &current_process_token)) {
        cout << "Failed to retrieve process token because " << GetLastError();
        return;
    }

    if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &privilege_id)) {
        cout << "Failed to lookup privilege because " << GetLastError();
        CloseHandle(current_process_token);
        return;
    }
    privileges.PrivilegeCount = 1;
    privileges.Privileges[0].Luid = privilege_id;
    privileges.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    if (!AdjustTokenPrivileges(current_process_token, FALSE, &privileges, sizeof(TOKEN_PRIVILEGES), NULL, NULL)) {
        cout << "Failed to adjust privileges because " << GetLastError();
        CloseHandle(current_process_token);
    }
}

void draw_tree(int root, int level) {
    for (int i = 0; i <= level; ++i)
        printf("\t");
    printf("%s %d\n", children_names[root], root);
    for (int i : parents_children[root])
        draw_tree(i, level + 1);
}


void kill_tree(int pid) {
    for (int i : parents_children[pid]) {
        kill_tree(i);
    }

    HANDLE process_to_terminate;
    process_to_terminate = OpenProcess(PROCESS_TERMINATE,
                                       FALSE,
                                       pid
                           );
    if (process_to_terminate == NULL) {
        cout << "Failed to retrive a handle for the process with pid " << pid << " because " << GetLastError() << '\n';
        return;
    }

    if (!TerminateProcess(process_to_terminate, 0)) {
        cout << "Failed to terminate the process with pid " << pid << " because " << GetLastError() << '\n';
    }
    else {
        cout << "Killed process with pid " << pid << '\n';
    }

    CloseHandle(process_to_terminate);
}

int main() {
    set_up_privileges();
    HANDLE hMapFile;
    LPCTSTR pBuf;

    hMapFile = OpenFileMapping(
            FILE_MAP_ALL_ACCESS,   // read/write access
            FALSE,                 // do not inherit the name
            szName);               // name of mapping object

    if (hMapFile == NULL) {
        _tprintf(TEXT("Could not open file mapping object (%d).\n"), GetLastError());
        return 1;
    }

    pBuf = (LPTSTR) MapViewOfFile(hMapFile, // handle to map object
                                  FILE_MAP_ALL_ACCESS,  // read/write permission
                                  0,
                                  0,
                                  BUF_SIZE);

    if (pBuf == NULL) {
        _tprintf(TEXT("Could not map view of file (%d).\n"), GetLastError());
        CloseHandle(hMapFile);
        return 1;
    }

    char *memory_buffer_copy = (char *) malloc(strlen(pBuf));
    strcpy(memory_buffer_copy, pBuf);
    set<int> processes;
    char *p = strtok(memory_buffer_copy, "\n");
    while (p) {
        int parent, child;
        char *name = (char *) malloc(strlen(p));
        sscanf_s(p, "%d,%d,%s", &parent, &child, name);
        children_names.insert(pair<int, char *>(child, name));
        processes.insert(child);
        if(parent != child){
            children_parents.insert(pair<int, int>(child, parent));
            parents_children[parent].push_back(child);
        }

        p = strtok(nullptr, "\n");
    }

    map<int, char*>::iterator itr3;
    for (itr3 = children_names.begin(); itr3 != children_names.end(); ++itr3) {
        continue;
    }

    map<int, int>::iterator itr;
    for (itr = children_parents.begin(); itr != children_parents.end(); ++itr) {
        if (processes.find(itr->second) == processes.end())
            roots.insert(itr->first);
    }
    cout << roots.size() << endl;
    int index = 1;
    for (int i : roots) {
        cout << index << endl;
        draw_tree(i, 0);
        index++;
    }

    int to_kill;
    cout << "Choose the tree to kill (the number of the tree): ";
    cin >> to_kill;
    for (int pid: roots) {
        to_kill--;
        if (!to_kill) {
            kill_tree(pid);
        }
    }
    UnmapViewOfFile(pBuf);
    CloseHandle(hMapFile);
    return 0;
}
