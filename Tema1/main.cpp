#include <Windows.h>
#include <tchar.h>
#include <stdio.h>
#include <strsafe.h>
#include <cstring>
#pragma comment(lib, "User32.lib")


void DisplayErrorBox(LPTSTR lpszFunction);
DWORD parseDirectory(LPSTR pathDirectory, LPSTR registryPath);
BOOLEAN createRegistryKey(HKEY baseKey, LPCSTR keyRoot);


HKEY hKey;

DWORD parseDirectory(LPSTR pathDirectory, LPSTR registryPath) {
    WIN32_FIND_DATA ffd;
    TCHAR currentDirectory[MAX_PATH];
    HANDLE hFind;
    DWORD dwError = 0;

    // Prepare string for use with FindFile functions.  First, copy the
    // string to a buffer, then append '\*' to the directory name.

    sprintf_s(currentDirectory, MAX_PATH, "%s\\*", pathDirectory);


    // Find the first file in the directory.

    hFind = FindFirstFile(currentDirectory, &ffd);

    if (hFind == INVALID_HANDLE_VALUE) {
        DisplayErrorBox(TEXT("FindFirstFile"));
        return dwError;
    }

    // List all the files in the directory

    do {
        if (ffd.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY) {
            if (strcmp(ffd.cFileName, ".") != 0 && strcmp(ffd.cFileName, "..") != 0) {
                TCHAR newDirectory[MAX_PATH];
                TCHAR newKeyEntry[MAX_PATH];
                sprintf_s(newDirectory, MAX_PATH, "%s\\%s", pathDirectory, ffd.cFileName);
                sprintf_s(newKeyEntry, MAX_PATH, "%s\\%s", registryPath, ffd.cFileName);
                createRegistryKey(HKEY_CURRENT_USER, newKeyEntry);
                parseDirectory(newDirectory, newKeyEntry);
            }
        } else {
            TCHAR newFile[MAX_PATH];
            sprintf_s(newFile, MAX_PATH, "%s\\%s", pathDirectory, ffd.cFileName);
            LSTATUS response = RegSetKeyValue(HKEY_CURRENT_USER, registryPath, ffd.cFileName, REG_DWORD, &ffd.nFileSizeLow, sizeof(DWORD));
            if (response == ERROR_SUCCESS) {
                printf("[i] %s: %s inserted with %ld value\n", registryPath, ffd.cFileName, ffd.nFileSizeLow);
            } else {
                printf("[i] Couldn't insert %s: %s with value %ld\n", registryPath, ffd.cFileName, ffd.nFileSizeLow);
            }
        }
    } while (FindNextFile(hFind, &ffd) != 0);

    dwError = GetLastError();
    if (dwError != ERROR_NO_MORE_FILES) {
        DisplayErrorBox(TEXT("FindFirstFile"));
    }

    FindClose(hFind);
    return dwError;
}


void DisplayErrorBox(LPTSTR lpszFunction) {
    // Retrieve the system error message for the last-error code

    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError();

    FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER |
            FORMAT_MESSAGE_FROM_SYSTEM |
            FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL,
            dw,
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPTSTR) &lpMsgBuf,
            0, NULL);

    // Display the error message and clean up

    lpDisplayBuf = (LPVOID) LocalAlloc(LMEM_ZEROINIT,
                                       (lstrlen((LPCTSTR) lpMsgBuf) + lstrlen((LPCTSTR) lpszFunction) + 40) *
                                       sizeof(TCHAR));
    StringCchPrintf((LPTSTR) lpDisplayBuf,
                    LocalSize(lpDisplayBuf) / sizeof(TCHAR),
                    TEXT("%s failed with error %d: %s"),
                    lpszFunction, dw, lpMsgBuf);
    MessageBox(NULL, (LPCTSTR) lpDisplayBuf, TEXT("Error"), MB_OK);

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
}


LPSTR getDirName(LPSTR path) {
    int pos;
    for (pos = strlen(path) - 1; pos >= 0 && path[pos] != '\\'; --pos);
    pos++;
    LPSTR dirName = new CHAR[MAX_PATH];
    strncpy_s(dirName, MAX_PATH, path + pos, strlen(path) - pos);
    return dirName;
}

BOOLEAN createRegistryKey(HKEY baseKey, LPCSTR keyRoot) {
    LSTATUS response = RegOpenKeyEx(
            baseKey,
            keyRoot,
            0,
            KEY_WRITE,
            &hKey
            );

    if (response == ERROR_SUCCESS) {
        printf("[i] %s already exists\n", keyRoot);
        return TRUE;
    }

    response = RegCreateKeyEx(
            baseKey,
            keyRoot,
            0,
            NULL,
            REG_OPTION_NON_VOLATILE,
            KEY_WRITE,
            NULL,
            &hKey,
            NULL
    );

    if (response != ERROR_SUCCESS) {
        printf("[x] Error at creating %s key registry\n", keyRoot);
        return FALSE;
    } else {
        printf("[i] %s key has been created\n", keyRoot);
        return TRUE;
    }
}

int main(int argc, TCHAR *argv[]) {

    size_t length_of_arg;
    LPCSTR keyRoot = "Software\\CSSO";

    // Check the number of parameters entered on input is right

    if (argc != 2) {
        _tprintf(TEXT("\n[x] Usage: %s <directory name>\n"), argv[0]);
        return (-1);
    }

    // Check that the input path plus 3 is not longer than MAX_PATH.
    // Three characters are for the "\*" plus NULL appended below.

    StringCchLength(argv[1], MAX_PATH, &length_of_arg);

    if (length_of_arg > (MAX_PATH - 3)) {
        _tprintf(TEXT("\n[x] Directory path is too long.\n"));
        return (-1);
    }

    _tprintf(TEXT("[i] Target directory is %s\n"), argv[1]);
    createRegistryKey(HKEY_CURRENT_USER, keyRoot);
    LPSTR dirName = getDirName(argv[1]);
    LPSTR dirRegKey = new CHAR[MAX_PATH];
    sprintf_s(dirRegKey, MAX_PATH, "%s\\%s", keyRoot, dirName);
    createRegistryKey(HKEY_CURRENT_USER, dirRegKey);
    printf("[i] Start\n\n");
    parseDirectory(argv[1], dirRegKey);
    return 0;
}